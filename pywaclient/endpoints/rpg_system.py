#    Copyright 2020 Jonas Waeber
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
from typing import Dict, Any, List

from pywaclient.endpoints import BasicEndpoint


class RpgSystemCrudEndpoint(BasicEndpoint):

    def __init__(self, client: 'BoromirApiClient'):
        super().__init__(client, 'rpgsystem')
        self.list_path = 'rpgsystems'

    def list(self) -> List[Dict[str, Any]]:
        """Get a list of all the RPG Systems present on World Anvil.
        """
        return self._post_request(self.list_path, {}, {})['entities']